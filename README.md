# create_jail

Исправь homedir в скрипте sr.sh ( по умолчанию homedir='/opt/chroot')

Для создания окружения запусть 
```
./sr.sh create_chroot
```

Пользователей необходимо создать примерно вот так
```
useradd -m -s /bin/bash <user>
```

после создания вех пользователей нужно их синхронизировать
```
./sr.sh update_users
```
Также необходимо изменить (сделать запись для каждого пользователя) /etc/ssh/sshd_config
```
Match User <user>
    ChrootDirectory <homedir из sr.sh>
```
и перезапутить sshd