#!/bin/bash

###new user
#username='test1'
#echo "setting user $username"
#useradd -m -s /bin/bash $username
#passwd $username
###
homedir='/opt/chroot'
if [ -n "$1" ]
    then if [[ "create_chroot" == "$1" ]]
             then
            mkdir -p $homedir/dev/
            cd $homedir/dev/
            mknod -m 666 null c 1 3
            mknod -m 666 tty c 5 0
            mknod -m 666 zero c 1 5
            mknod -m 666 random c 1 8
            mknod -m 666 urandom c 1 9
            chown root:root $homedir
            chmod 0755 $homedir
            mkdir -p $homedir/bin
            mkdir $homedir/etc

            ###если нужно добавить программы добавляй в строке ниже
            for prog in "/usr/bin/ssh /bin/bash /bin/ping /usr/bin/snmpget /usr/bin/snmpwalk /usr/bin/telnet /usr/bin/traceroute"
            ###
            do
                cp -v $prog $homedir/bin/

                for i in `ldd $prog | awk '{print $1}'`;
                    do
                        if [[ $i == *"/"* ]];
                            then mkdir -p $homedir/$i; rm -r $homedir/$i;
                                cp -v $i $homedir/$i
                        fi;
                done

                for i in `ldd $prog | awk '{print $3}'`;
                    do
                        if [[ $i == *"/"* ]];
                            then mkdir -p $homedir/$i; rm -r $homedir/$i;
                                cp -v $i $homedir/$i
                        fi;
                done
            done
            chmod +s $homedir/bin/ping
            mkdir -p $homedir/usr/share/snmp
            mkdir -p $homedir/var/lib
            cp -r /var/lib/snmp $homedir/var/lib
            cp -r /usr/share/snmp $homedir/usr/share
            cp /lib/x86_64-linux-gnu/libnss* $homedir/lib/x86_64-linux-gnu/
            cp /etc/hosts $homedir/etc
            cp /etc/resolv.conf $homedir/etc
            cp /etc/services $homedir/etc/

            elif [[ "update_users" == "$1" ]]
                    then
                    ###copy users pass and groups
                    cp -vf /etc/{passwd,group} $homedir/etc/
                    ###

            else echo "use $0 create_chroot
use $0 update_users"
            fi
else echo "use $0 create_chroot
use $0 update_users"
fi